import 'dart:io';

import 'package:dart_application_2/dart_application_2.dart' as dart_application_2;
import 'package:dart_application_2/dart_application_2.dart';

import 'Calculate.dart';

void main(List<String> arguments) {
  var x = new Calculate();
  print('Welcome to calculator');
  print('Enter first number: ');
  var n1 = int.parse(stdin.readLineSync()!);
  print('Enter second number: ');
  var n2 = int.parse(stdin.readLineSync()!);

  String choice = '''
  Select Operation
  1. Add
  2. Sub
  3. Mul
  4. Div
  Your choice ? ''';

  stdout.write(choice);
  var choose = int.parse(stdin.readLineSync()!);

  if(choose == 1){
    print('Sum = ${x.add(n1,n2)}');
  }
  if(choose == 2){
    print('Difference = ${x.sub(n1,n2)}');
  }
  if(choose == 3){
    print('Product = ${x.mul(n1,n2)}');
  }
  if(choose == 4){
    print('Quotient = ${x.div(n1,n2)}');
  }
}